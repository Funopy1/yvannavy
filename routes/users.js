const express = require('express')
const router = express.Router()
const UserController = require('../controllers/users')
const auth = require('../auth')

//register
router.post('/', (req, res) => {
    UserController.register(req.body).then(result => res.send(result))
})

// Login
router.post('/login', (req, res) => {
    UserController.login(req.body).then(result => res.send(result))
})

// Details
router.get('/details', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
    UserController.get({ userId: user.id }).then(user => res.send(user))
})

module.exports = router; 