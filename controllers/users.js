const User = require('../models/users') 
const bcrypt = require('bcryptjs')
const auth = require('../auth')

module.exports.register = (params) => {
	let user = new User({
		username: params.username,
		password: bcrypt.hashSync(params.password, 10),				// para nakarandom yung password eto yung giangawa ni bcypt
		isAdmin : params.isAdmin
	})
	return user.save().then((user, err) => {
		return (err) ? false : true
	})

}

module.exports.login = (params) => {
	return User.findOne({ username: params.username }).then(user => {
		if (user === null) { 
			return { error: 'does-not-exist'} 
		}
		
		const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

		if (isPasswordMatched) {
			return { accessToken: auth.createAccessToken(user.toObject()) }
		} else {
			return { error: 'incorrect-password'}
		}
	})
}


// Details
module.exports.get = (params) => {
	return User.findById(params.userId).then(user => {
		user.password = undefined
		return user
	})
}